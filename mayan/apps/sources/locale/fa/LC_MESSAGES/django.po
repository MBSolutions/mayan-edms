# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Mehdi Amani <MehdiAmani@toorintan.com>, 2018
# Mohammad Dashtizadeh <mohammad@dashtizadeh.net>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:32-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Persian (http://www.transifex.com/rosarior/mayan-edms/language/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:40 links.py:56 models.py:64 queues.py:8 settings.py:10 views.py:627
msgid "Sources"
msgstr "منابع"

#: apps.py:55
msgid "Create a document source"
msgstr "یک منبع مستند ایجاد کنید"

#: apps.py:57
msgid ""
"Document sources are the way in which new documents are feed to Mayan EDMS, "
"create at least a web form source to be able to upload documents from a "
"browser."
msgstr "منابع سندي روشي است که اسناد جديد به EDMS ميان تغيير مي کند و حداقل يک منبع فرم وب را قادر مي سازد تا اسناد را از يک مرورگر آپلود کند."

#: apps.py:67
msgid "Created"
msgstr "ایجاد شده"

#: apps.py:74
msgid "Thumbnail"
msgstr "بند انگشتی"

#: apps.py:82 models.py:789
msgid "Date time"
msgstr "زمان قرار"

#: apps.py:87 models.py:792
msgid "Message"
msgstr "پیام"

#: forms.py:30
msgid "Comment"
msgstr "اظهار نظر"

#: forms.py:45
msgid "Expand compressed files"
msgstr "فایلهای فشرده را گسترش دهید"

#: forms.py:47
msgid "Upload a compressed file's contained files as individual documents"
msgstr "فایل های حاوی فایل فشرده را به عنوان اسناد شخصی بارگذاری کنید"

#: forms.py:68 views.py:483
msgid "Staging file"
msgstr "مرحله گذاری فایل"

#: forms.py:72 forms.py:77
msgid "File"
msgstr "فایل"

#: handlers.py:16
msgid "Default"
msgstr "پیش فرض"

#: links.py:51
msgid "New document"
msgstr "سند جدید"

#: links.py:62
msgid "Add new IMAP email"
msgstr "افزودن ایمیل جدید IMAP"

#: links.py:67
msgid "Add new POP3 email"
msgstr "افزودن ایمیل جدید POP3"

#: links.py:72
msgid "Add new staging folder"
msgstr "افزودن پوشه مرحلهبندی جدید"

#: links.py:77
msgid "Add new watch folder"
msgstr "پوشه تماشا را اضافه کنید"

#: links.py:82
msgid "Add new webform source"
msgstr "منبع جدید وب فرم را اضافه کنید"

#: links.py:87
msgid "Add new SANE scanner"
msgstr "افزودن اسکنر SANE جدید"

#: links.py:92 links.py:106
msgid "Delete"
msgstr "حذف"

#: links.py:96
msgid "Edit"
msgstr "ویرایش"

#: links.py:100
msgid "Document sources"
msgstr "منابع سند"

#: links.py:111
msgid "Upload new version"
msgstr "آپلود نسخه جدید"

#: links.py:115
msgid "Logs"
msgstr "سیاههها"

#: links.py:120
msgid "Check now"
msgstr "اکنون بررسی کنید"

#: literals.py:16
msgid "Flatbed"
msgstr "صفحه تخت"

#: literals.py:17
msgid "Document feeder"
msgstr "فیدر سند"

#: literals.py:24
msgid "Simplex"
msgstr "سیمپلکس"

#: literals.py:25
msgid "Duplex"
msgstr "دوبلکس"

#: literals.py:33
msgid "Lineart"
msgstr "خط مقدم"

#: literals.py:34
msgid "Monochrome"
msgstr "تک رنگ"

#: literals.py:35
msgid "Color"
msgstr "رنگ"

#: literals.py:43 literals.py:48
msgid "Always"
msgstr "همیشه"

#: literals.py:44 literals.py:49
msgid "Never"
msgstr "هرگز"

#: literals.py:50
msgid "Ask user"
msgstr "از کاربر بپرس"

#: literals.py:61
msgid "Scanner"
msgstr "اسکنر"

#: literals.py:62 models.py:393
msgid "Web form"
msgstr "فرم وب"

#: literals.py:63 models.py:329
msgid "Staging folder"
msgstr "پوشه Staging"

#: literals.py:64 models.py:765
msgid "Watch folder"
msgstr "پوشه تماشا کنید"

#: literals.py:65
msgid "POP3 email"
msgstr "ایمیل POP3"

#: literals.py:66 models.py:662 models.py:663
msgid "IMAP email"
msgstr "ایمیل IMAP"

#: models.py:55
msgid "Label"
msgstr "برچسب"

#: models.py:57 views.py:592
msgid "Enabled"
msgstr "فعال شده است"

#: models.py:63 models.py:786
msgid "Source"
msgstr "منبع"

#: models.py:176
msgid "Interactive source"
msgstr "منبع تعاملی"

#: models.py:177
msgid "Interactive sources"
msgstr "منابع تعاملی"

#: models.py:187
msgid "Device name as returned by the SANE backend."
msgstr "نام دستگاه به عنوان پشت صحنه SANE بازگشت."

#: models.py:188
msgid "Device name"
msgstr "نام دستگاه"

#: models.py:193
msgid ""
"Selects the scan mode (e.g., lineart, monochrome, or color). If this option "
"is not supported by your scanner, leave it blank."
msgstr "حالت اسکن را انتخاب می کند (به عنوان مثال، خط، سیاه و سفید یا رنگ). اگر این گزینه توسط اسکنر شما پشتیبانی نمی شود، آن را خالی بگذارید."

#: models.py:195
msgid "Mode"
msgstr "حالت"

#: models.py:199
msgid ""
"Sets the resolution of the scanned image in DPI (dots per inch). Typical "
"value is 200. If this option is not supported by your scanner, leave it "
"blank."
msgstr "وضوح تصویر اسکن شده را در DPI (نقطه در اینچ) تنظیم می کند. مقدار معمول 200 است. اگر این گزینه توسط اسکنر شما پشتیبانی نمی شود، آن را خالی بگذارید."

#: models.py:202
msgid "Resolution"
msgstr "وضوح"

#: models.py:206
msgid ""
"Selects the scan source (such as a document-feeder). If this option is not "
"supported by your scanner, leave it blank."
msgstr "منبع اسکن (مانند فیدر سند) را انتخاب می کند. اگر این گزینه توسط اسکنر شما پشتیبانی نمی شود، آن را خالی بگذارید."

#: models.py:208
msgid "Paper source"
msgstr "منبع کاغذ"

#: models.py:213
msgid ""
"Selects the document feeder mode (simplex/duplex). If this option is not "
"supported by your scanner, leave it blank."
msgstr "حالت فیدر سند (simplex / duplex) را انتخاب می کند. اگر این گزینه توسط اسکنر شما پشتیبانی نمی شود، آن را خالی بگذارید."

#: models.py:215
msgid "ADF mode"
msgstr "حالت ADF"

#: models.py:221
msgid "SANE Scanner"
msgstr "SANE اسکنر"

#: models.py:222
msgid "SANE Scanners"
msgstr "SANE اسکنرها"

#: models.py:269
#, python-format
msgid "Error while scanning; %s"
msgstr "خطا هنگام اسکن کردن؛ %s"

#: models.py:301 models.py:758
msgid "Server side filesystem path."
msgstr "محل قرارگیری برروی سیستم فایل سمت سرور"

#: models.py:302 models.py:759
msgid "Folder path"
msgstr "محل پرونده"

#: models.py:305
msgid "Width value to be passed to the converter backend."
msgstr "مقداری که به مبدل جهت عرض ارسال خواهد شد."

#: models.py:306
msgid "Preview width"
msgstr "عرض پیش بینی"

#: models.py:310
msgid "Height value to be passed to the converter backend."
msgstr "مقداری که به مبدل جهت ارتفاع ارسال خواهد شد."

#: models.py:311
msgid "Preview height"
msgstr "ارتفاع پیش بینی"

#: models.py:315 models.py:386
msgid "Whether to expand or not compressed archives."
msgstr "گسترش و یا آرشیوهای غیر فشرده"

#: models.py:316 models.py:387 models.py:427
msgid "Uncompress"
msgstr "فشرده نشده"

#: models.py:321
msgid "Delete the file after is has been successfully uploaded."
msgstr "حدف فایل پس از آپلود موفق آن."

#: models.py:323
msgid "Delete after upload"
msgstr "حذف پس ار آپ لود"

#: models.py:330
msgid "Staging folders"
msgstr "پرونده های مرحله ای"

#: models.py:342
#, python-format
msgid "Error deleting staging file; %s"
msgstr "خطای حذف فایل مرحله ای : %s"

#: models.py:358
#, python-format
msgid "Unable get list of staging files: %s"
msgstr "قادر به گرفتن لیست فایلهای مرحله ای نیست. %s"

#: models.py:394
msgid "Web forms"
msgstr "فرمهای وب"

#: models.py:407 models.py:408
msgid "Out of process"
msgstr "خارج از پردازش"

#: models.py:414
msgid "Interval in seconds between checks for new documents."
msgstr "مدت زمان بین بررسی جهت سند جدید."

#: models.py:415
msgid "Interval"
msgstr "فاصله"

#: models.py:420
msgid "Assign a document type to documents uploaded from this source."
msgstr "این نوع را به اسناد آپلود شده از این آدرس تخصیص دهید."

#: models.py:422
msgid "Document type"
msgstr "نوع سند"

#: models.py:426
msgid "Whether to expand or not, compressed archives."
msgstr "غییر فشرده سازی آرشیوهای فشرده شده: بلی خیر"

#: models.py:433
msgid "Interval source"
msgstr "فاصله سورس"

#: models.py:434
msgid "Interval sources"
msgstr "فاصله سورسها"

#: models.py:492
msgid "Host"
msgstr "هاست"

#: models.py:493
msgid "SSL"
msgstr "SSL"

#: models.py:495
msgid ""
"Typical choices are 110 for POP3, 995 for POP3 over SSL, 143 for IMAP, 993 "
"for IMAP over SSL."
msgstr "Typical choices are 110 for POP3, 995 for POP3 over SSL, 143 for IMAP, 993 for IMAP over SSL."

#: models.py:496
msgid "Port"
msgstr "Port"

#: models.py:498
msgid "Username"
msgstr "نام کاربری"

#: models.py:499
msgid "Password"
msgstr "کلمه عبور"

#: models.py:503
msgid ""
"Name of the attachment that will contains the metadata type names and value "
"pairs to be assigned to the rest of the downloaded attachments. Note: This "
"attachment has to be the first attachment."
msgstr "نام پیوست که شامل نام های نوع فراداده و جفت های ارزش است که باید به سایر فایل های دانلود شده اختصاص داده شود. توجه: این پیوست باید اولین ضمیمه باشد."

#: models.py:507
msgid "Metadata attachment name"
msgstr "نام دلبستگی متاداده"

#: models.py:511
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's subject."
msgstr "نوع متادیتایی معتبر برای نوع سند انتخاب شده که در آن برای ذخیره موضوع ایمیل انتخاب کنید."

#: models.py:514
msgid "Subject metadata type"
msgstr "نوع Metadata موضوع"

#: models.py:518
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's \"from\" value."
msgstr "نوع متادیتایی معتبر برای نوع سند انتخاب شده که در آن برای ذخیره \"ایمیل\" از \"ارزش\" را انتخاب کنید."

#: models.py:521
msgid "From metadata type"
msgstr "از نوع ابرداده"

#: models.py:525
msgid "Store the body of the email as a text document."
msgstr "بدن ایمیل را به عنوان یک سند متن ذخیره کنید."

#: models.py:526
msgid "Store email body"
msgstr "فروشگاه الکترونیکی ذخیره کنید"

#: models.py:532
msgid "Email source"
msgstr "ایمیل کردن سورس"

#: models.py:533
msgid "Email sources"
msgstr "ایمیل کردن سورسها"

#: models.py:541
#, python-format
msgid ""
"Subject metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr "نوع Metadata موضوع \"%(metadata_type)s\" برای نوع سند معتبر نیست: %(document_type)s"

#: models.py:555
#, python-format
msgid ""
"\"From\" metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr "\"از\" نوع ابرداده \"%(metadata_type)s\" برای نوع سند معتبر نیست: %(document_type)s"

#: models.py:655
msgid "IMAP Mailbox from which to check for messages."
msgstr "صندوق پستی IMAP که از آن برای بررسی پیام ها است."

#: models.py:656
msgid "Mailbox"
msgstr "صندوق پستی"

#: models.py:701
msgid "Timeout"
msgstr "اتمام وقت"

#: models.py:707 models.py:708
msgid "POP email"
msgstr "POP ایمیل"

#: models.py:766
msgid "Watch folders"
msgstr "پرونده تحت نظر"

#: models.py:798
msgid "Log entry"
msgstr "ورودی لاگ"

#: models.py:799
msgid "Log entries"
msgstr "ورودیهای لاگ"

#: permissions.py:7
msgid "Sources setup"
msgstr "راه اندازی ویا نصب منابع "

#: permissions.py:9
msgid "Create new document sources"
msgstr "ایجاد سورس سند جدید"

#: permissions.py:12
msgid "Delete document sources"
msgstr "حذف سورس سند"

#: permissions.py:15
msgid "Edit document sources"
msgstr "ویرایش سورس سند"

#: permissions.py:18
msgid "View existing document sources"
msgstr "دیدن سورس اسناد موجود"

#: permissions.py:21
msgid "Delete staging files"
msgstr "حذف فایل های استقراری"

#: queues.py:11
msgid "Sources periodic"
msgstr "منابع دوره ای"

#: queues.py:14
msgid "Sources fast"
msgstr ""

#: queues.py:19
msgid "Generate staging file image"
msgstr ""

#: queues.py:23
msgid "Check interval source"
msgstr "بررسی منبع فاصله"

#: queues.py:27
msgid "Handle upload"
msgstr "آپلود دسته"

#: queues.py:31
msgid "Upload document"
msgstr "سند آپلود"

#: settings.py:15
msgid "File path to the scanimage program used to control image scanners."
msgstr "مسیر فایل به برنامه scanimage مورد استفاده برای کنترل اسکنر تصویر."

#: settings.py:22
msgid ""
"Path to the Storage subclass to use when storing the cached staging_file "
"image files."
msgstr ""

#: settings.py:31
msgid "Arguments to pass to the SOURCES_STAGING_FILE_CACHE_STORAGE_BACKEND."
msgstr ""

#: tasks.py:46
#, python-format
msgid "Error processing source: %s"
msgstr "خطای پردازش منبع: %s"

#: views.py:66
msgid ""
"Any error produced during the usage of a source will be listed here to "
"assist in debugging."
msgstr ""

#: views.py:69
msgid "No log entries available"
msgstr ""

#: views.py:71
#, python-format
msgid "Log entries for source: %s"
msgstr "ورود به سیستم برای منبع: %s"

#: views.py:127 wizards.py:145
msgid ""
"No interactive document sources have been defined or none have been enabled,"
" create one before proceeding."
msgstr "هیچ منبع محاوره ای سند تعریف و یا فعال نشده، قبل از ادامه دادن یک منبع بسازید."

#: views.py:153 views.py:171 views.py:181
msgid "Document properties"
msgstr "خواص سند"

#: views.py:161
msgid "Files in staging path"
msgstr "فایلهای درون راه مرحله ای"

#: views.py:172
msgid "Scan"
msgstr "اسکن کردن"

#: views.py:281
#, python-format
msgid ""
"Error executing document upload task; %(exception)s, %(exception_class)s"
msgstr ""

#: views.py:295
msgid "New document queued for upload and will be available shortly."
msgstr ""

#: views.py:346
#, python-format
msgid "Upload a document of type \"%(document_type)s\" from source: %(source)s"
msgstr ""

#: views.py:379
#, python-format
msgid "Document \"%s\" is blocked from uploading new versions."
msgstr "سند \"%s\" از آپلود نسخه های جدید مسدود شده است."

#: views.py:431
msgid "New document version queued for upload and will be available shortly."
msgstr ""

#: views.py:470
#, python-format
msgid "Upload a new version from source: %s"
msgstr "آپلود نسخه ای جدید از اصل : %s"

#: views.py:510
#, python-format
msgid "Trigger check for source \"%s\"?"
msgstr "برای اطمینان از منبع \"%s\" تکرار کنید؟"

#: views.py:523
msgid "Source check queued."
msgstr "بررسی منبع در صف."

#: views.py:537
#, python-format
msgid "Create new source of type: %s"
msgstr "ایجاد سورس جدید از نوع %s."

#: views.py:557
#, python-format
msgid "Delete the source: %s?"
msgstr "منبع را حذف کنید: %s؟"

#: views.py:576
#, python-format
msgid "Edit source: %s"
msgstr "ویرایش اصل : %s"

#: views.py:588
msgid "Type"
msgstr "نوع"

#: views.py:621
msgid ""
"Sources provide the means to upload documents. Some sources like the "
"webform, are interactive and require user input to operate. Others like the "
"email sources, are automatic and run on the background without user "
"intervention."
msgstr ""

#: views.py:626
msgid "No sources available"
msgstr ""

#: wizards.py:96
msgid "Select document type"
msgstr ""

#: wizards.py:164
#, python-format
msgid "Step %(step)d of %(total_steps)d: %(step_label)s"
msgstr ""

#: wizards.py:169
msgid "Next step"
msgstr "مرحله بعدی"

#: wizards.py:171
msgid "Document upload wizard"
msgstr "جادوگر بارگذاری سند"
